# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 17:20:10 2019

@author: jingf
"""

import numpy as np
def gradient_4point(f, dx):
    """
    The gradient of array f assuming points are a distance dx apart
    using 4-point differences
    """
    dfdx = np.zeros_like(f)
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx
    dfdx[-2] = (f[-2]-f[-3])/dx
    dfdx[1] = (f[2]-f[1])/dx
    
    for i in range(2,len(f)-2):
        dfdx[i] = (-f[i+2]+8*f[i+1] - 8*f[i-1]+f[i-2])/(12*dx)
    return dfdx