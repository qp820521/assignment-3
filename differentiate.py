# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:39:42 2019

@author: qp820521
"""

import numpy as np
def gradient_2point(f, dx):
    """
    The gradient of array f assuming points are a distance dx apart
    using 2-point differences
    """
    dfdx = np.zeros_like(f)
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx
    
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx