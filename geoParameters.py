# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:13:09 2019

@author: qp820521
"""

import numpy as np
pa = 1e5
pb = 200.
f = 1e-4
rho = 1.
L = 2.4e6
ymin = 0.0
ymax = 1e6
def pressure(y):
    "The pressure and given y locations"
    return pa + pb*np.cos(y*np.pi/L)

def uExact(y):
    "The analytic geostrophic wind at given location, y"
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy):
    "The geostrophic wind as a function of pressure gradient"
    return -dpdy/(rho*f)

