# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 15:21:21 2019

@author: qp820521
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *

def geostrophicWind():
    import geoParameters as gp
    N = 10
    dy = (gp.ymax - gp.ymin)/N
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    uExact = gp.uExact(y)
    p = gp.pressure(y)
    dpdy = gradient_2point(p, dy)
    u_2point = gp.geoWind(dpdy)
    
    font = {'size' : 14}
    plt.rc('font' , **font)
    
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label='Two-point differences', \
             ms = 12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y(km)')
    plt.ylabel('u(m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCent.pdf')
    plt.show()
    
    uError = abs(u_2point - uExact)
    plt.plot(y/1000,uError,'k-',label = 'Error')
    plt.legend(loc='best')
    plt.xlabel('y(km)')
    plt.ylabel('u(m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCenterror.pdf')
    plt.show()
    
    
    #Y = sin(x)
    #for i in range(0,3)     
if __name__ == "__main__":
    geostrophicWind()
    


