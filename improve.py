# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 17:19:07 2019

@author: jingf
"""

import numpy as np
import matplotlib.pyplot as plt
from differen2 import *

def geostrophicWind():
    import geoParameters as gp
    N = 10
    dy = (gp.ymax - gp.ymin)/N
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    uExact = gp.uExact(y)
    p = gp.pressure(y)
    dpdy = gradient_4point(p, dy)
    u_4point = gp.geoWind(dpdy)
    
    font = {'size' : 14}
    plt.rc('font' , **font)
    
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_4point, '*k--', label='Four-point differences', \
             ms = 12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y(km)')
    plt.ylabel('u(m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCentimprove.pdf')
    plt.show()
    
    uError2 = abs(u_4point - uExact)
    plt.plot(y/1000,uError2,'k-',label = 'Error2')
    plt.legend(loc='best')
    plt.xlabel('y(km)')
    plt.ylabel('u(m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCenterror2.pdf')
    plt.show()
    
    

if __name__ == "__main__":
    geostrophicWind()
    